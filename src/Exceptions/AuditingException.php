<?php

namespace Ensi\LaravelEnsiAudit\Exceptions;

use Exception;

class AuditingException extends Exception
{
}
